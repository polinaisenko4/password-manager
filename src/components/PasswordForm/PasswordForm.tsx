import "./PasswordForm.css";

interface PasswordFormProps {
  values: {
    account: string;
    password: string;
  };
  setValue: (key: string, value: string) => void;
  clickForm: React.MouseEventHandler<HTMLButtonElement>;
}

const PasswordForm: React.FC<PasswordFormProps> = ({
  setValue,
  values,
  clickForm,
}) => {
  return (
    <div>
      <form className="password-form">
        <div className="password-form-values">
          <p>Which account do you want to save the password from?</p>
          <input
            className="password-form-input"
            value={values.account}
            onChange={(e) => {
              setValue("account", e.target.value);
            }}
          />
        </div>
        <div className="password-form-values">
          <p>Password for the account</p>
          <input
            className="password-form-input"
            value={values.password}
            onChange={(e) => {
              setValue("password", e.target.value);
            }}
          />
        </div>
        <button onClick={clickForm} className="add-card">
          Add
        </button>
      </form>
    </div>
  );
};

export default PasswordForm;
