import "./AuthForm.css";

type AuthFormProps = {
  setValue: any;
  authType: string;
  values: {
    email: string;
    password: string;
  };
  clickForm: (e: any) => void;
  formError: string;
};

const AuthForm: React.FC<AuthFormProps> = ({
  setValue,
  values,
  authType,
  clickForm,
  formError,
}) => {
  return (
    <div className="auth-wrap">
      <div className="auth-banner">
        <h2 className="auth-title">
          <strong> {authType}</strong> on our PASSWORD MANAGER
        </h2>
        <p className="auth-desc">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Praesentium
          veniam dicta similique doloribus.
        </p>
      </div>
      <form>
        <p className="auth-error">{formError}</p>
        <div>
          <div>Email</div>
          <input
            className={
              formError === ""
                ? "auth-form-value"
                : " auth-form-value auth-form-value-invalid"
            }
            type="email"
            value={values.email}
            onChange={(e) => {
              setValue("email", e.target.value);
            }}
          />
        </div>
        <div>
          <div>Password</div>
          <input
            className={
              formError === ""
                ? "auth-form-value"
                : "auth-form-value auth-form-value-invalid"
            }
            type="password"
            value={values.password}
            onChange={(e) => {
              setValue("password", e.target.value);
            }}
          />
        </div>
        <div>
          <button className="auth-button" onClick={clickForm}>
            {authType}
          </button>
          <div className="auth-button-effect"></div>
        </div>
      </form>
    </div>
  );
};

export default AuthForm;
