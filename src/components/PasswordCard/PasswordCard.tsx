import { useState } from "react";

import { useAuth } from "../../providers/AuthProvider";
import "./PasswordCard.css";

interface PasswordCardProps {
  id: string;
  account: string;
  password: string;
  setDelete: () => void;
}

const PasswordCard: React.FC<PasswordCardProps> = ({
  id,
  account,
  password,
  setDelete,
}) => {
  const { updatePassword } = useAuth();

  const [accountInput, setAccountInput] = useState(account);
  const [passwordInput, setPasswordInput] = useState(password);
  const [passwordShown, setPasswordShown] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  return (
    <div className="passwordCard-wrap">
      <div className={!isEditing ? "editshown" : "editNoShown"}>
        <div className="delete-block">
          <button
            className="material-icons edit"
            onClick={() => setIsEditing(true)}
          >
            edit
          </button>
          <button className="material-icons delete" onClick={setDelete}>
            delete
          </button>
        </div>
      </div>
      <div className="passwordCard-account">
        <h4>Account:</h4>
        <input
          className="password-input"
          value={isEditing ? accountInput : account}
          type="text"
          readOnly={!isEditing}
          onChange={(e) => setAccountInput(e.target.value)}
        />
      </div>
      <div className="passwordCard-password">
        <h4>Password:</h4>
        {isEditing ? (
          <input
            className="password-input"
            value={passwordInput}
            type="password"
            onChange={(e) => setPasswordInput(e.target.value)}
          />
        ) : (
          <>
            <input
              className="password-input"
              value={passwordShown ? password : "********"}
              readOnly
            />
            <div
              className="material-icons visibility"
              onClick={() => setPasswordShown((val) => !val)}
            >
              {passwordShown ? "visibility_off" : "visibility"}
            </div>
          </>
        )}
      </div>
      {isEditing && (
        <div className="edit-block">
          <button
            className="cancel"
            onClick={() => {
              setAccountInput(account);
              setPasswordInput(password);
              setIsEditing(false);
            }}
          >
            Cancel
          </button>
          <button
            className="save"
            onClick={() => {
              updatePassword({
                id,
                account: accountInput,
                password: passwordInput,
              }).then(() => setIsEditing(false));
            }}
          >
            Save
          </button>
        </div>
      )}
    </div>
  );
};

export default PasswordCard;
