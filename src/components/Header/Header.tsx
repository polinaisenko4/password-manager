import "./Header.css";
import { useAuth } from "../../providers/AuthProvider";
import Logo from "../../UI/Logo/Logo";
import { useNavigate } from "react-router-dom";

const Header = () => {
  const { user, signOut } = useAuth();
  const navigate = useNavigate();

  console.log(user);

  return (
    <div className="header-wrap">
      <div className="container">
        <div className="header-flex">
          <div className="header-left">
            <Logo />
          </div>
          <div className="header-right">
            {!user && (
              <>
                <button
                  className="button"
                  onClick={() => {
                    navigate("/register");
                  }}
                >
                  Sign up
                </button>

                <button
                  className="button"
                  onClick={() => {
                    navigate("/login");
                  }}
                >
                  Sign in
                </button>
              </>
            )}
            {user && (
              <>
                <button
                  className="button"
                  onClick={() => {
                    navigate("/login");
                    signOut();
                  }}
                >
                  Exit
                </button>
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
