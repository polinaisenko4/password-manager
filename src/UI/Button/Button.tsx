import './Button.css'

interface ButtonProps {
  color: string;
  width: string;
  text: string;
  borderColor: string;
  onClick: () => void;
}

const Button: React.FC<ButtonProps> = ({ color, width, text, borderColor, onClick }) => {
  return (
    <div className='button-wrap'>
      <button className="button" onClick={() => onClick } style={{ backgroundColor: color, width: width, borderColor: borderColor}}>
        {text}
      </button>
    </div>
  );
};

export default Button;
