import React, { createContext, useState, useEffect, useContext } from "react";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut as firebaseSignOut,
} from "firebase/auth";
import {
  getDatabase,
  set,
  ref,
  onValue,
  remove,
  off,
  update,
} from "firebase/database";

interface AuthProviderProps {
  children: any;
}

interface PasswordEntity {
  id: string;
  account: string;
  password: string;
}

interface ContextValue {
  user?: any;
  passwords: PasswordEntity[];
  signUp: (email: string, password: string) => Promise<void>;
  signIn: (email: string, password: string) => Promise<void>;
  signOut: () => Promise<void>;
  addPassword: (data: PasswordEntity) => Promise<void>;
  removePassword: (id: string) => Promise<void>;
  updatePassword: (data: PasswordEntity) => Promise<void>;
}

export const AuthContext = createContext<ContextValue>({
  user: null,
  passwords: [],
  signUp: async (email: string, password: string) => {},
  signIn: async (email: string, password: string) => {},
  signOut: async () => {},
  addPassword: async (data: PasswordEntity) => {},
  removePassword: async (id: string) => {},
  updatePassword: async (data: PasswordEntity) => {},
});

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [u, setUser] = useState<any>({});
  const [passwords, setPassword] = useState<any>([]);

  const signUp = async (email: string, password: string) => {
    const auth = getAuth();
    try {
      const userCredential = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      setUser(userCredential.user);
    } catch (err) {
      console.error(err);
      throw err;
    }
  };

  const signIn = async (email: string, password: string) => {
    const auth = getAuth();
    try {
      const userCredential = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      setUser(userCredential.user);
    } catch (err) {
      console.error(err);
      throw err;
    }
  };

  const signOut = async () => {
    const auth = getAuth();
    try {
      await firebaseSignOut(auth);
      setUser(null);
    } catch (err) {
      console.error(err);
    }
  };

  const addPassword = async (data: PasswordEntity) => {
    if (u) {
      const db = getDatabase();
      set(ref(db, `passwords/${u.uid}/${data.id}`), data);
    }
  };

  const removePassword = async (id: string) => {
    if (u) {
      const db = getDatabase();
      remove(ref(db, `passwords/${u.uid}/${id}`));
    }
  };

  const updatePassword = async (data: PasswordEntity) => {
    if (u) {
      const db = getDatabase();
      update(ref(db, `passwords/${u.uid}/${data.id}`), data);
    }
  };

  useEffect(() => {
    const auth = getAuth();
    const db = getDatabase();
    onAuthStateChanged(auth, (user) => {
      console.log(user);
      if (user) {
        setUser(user);
        onValue(ref(db, `/passwords/${user.uid}`), (snapshot) => {
          const value = snapshot.val();
          setPassword(value);
        });
      } else {
        if (u && u.uid) {
          off(ref(db, `/passwords/${u.uid}`));
          setPassword([]);
        }
        setUser(null);
      }
    });
    // eslint-disable-next-line
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user: u,
        signUp,
        signIn,
        signOut,
        addPassword,
        removePassword,
        updatePassword,
        passwords,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);
  return context;
}
