import { useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthForm from "../../components/AuthForm/AuthForm";
import { useAuth } from "../../providers/AuthProvider";

const LoginPage = () => {
    let navigate = useNavigate();

  const [values, setValues] = useState({
    email: "",
    password: "",
  });

  const {signIn} = useAuth();
  const [error, setError] = useState('');

  
  const login = (e: any) => {
    e.preventDefault();
        signIn(values.email, values.password).then(() => {
           navigate('/dashboard');
        }).catch((err: Error) => {
            setError(err.message)
        })
    console.log('login done')
  };

  console.log(
      values
  )

  return (
    <AuthForm
      setValue={(name: string, value: string) => {
        setValues((v) => {
          return {
            ...v,
            [name]: value,
          };
        });
      }}
      values={values}
      authType="Login"
      clickForm={login}
      formError={error}
    />
  );
}

export default LoginPage;