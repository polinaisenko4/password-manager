import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthForm from "../../components/AuthForm/AuthForm";
import {useAuth } from "../../providers/AuthProvider";

const RegisterPage = () => {
  let navigate = useNavigate();

  const {user} = useAuth()

  const [values, setValues] = useState({
    email: "",
    password: "",
  });

  const {signUp} = useAuth();
  const [error, setError] = useState('');

  
  const register = (e: any) => {
    e.preventDefault();
        signUp(values.email, values.password).then(() => {
           navigate('/dashboard');
        }).catch((err: Error) => {
            setError(err.message)
        })
    console.log('register done', user)
  };

  console.log(
      values
  )

  return (
    <AuthForm
      setValue={(name: string, value: string) => {
        setValues((v) => {
          return {
            ...v,
            [name]: value,
          };
        });
      }}
      values={values}
      authType="Register"
      clickForm={register}
      formError={error}
    />
  );
};

export default RegisterPage;
