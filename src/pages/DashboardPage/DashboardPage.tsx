import {  useState } from "react";
import { useNavigate } from "react-router-dom";
import PasswordCard from "../../components/PasswordCard/PasswordCard";
import PasswordForm from "../../components/PasswordForm/PasswordForm";
import { useAuth } from "../../providers/AuthProvider";

import "./Dashboard.css";

const DashboardPage = () => {
  const navigate = useNavigate();
  const { user, addPassword, removePassword, passwords } = useAuth();
  const [showForm, setShowForm] = useState(false);
  const [values, setValues] = useState({
    account: "",
    password: "",
  });

  const addNewPassword = (e: any) => {
    e.preventDefault();
    const newPassword = {
      id: Date.now().toString(),
      password: values.password,
      account: values.account,
    };
    if (values.account !== "" && values.password !== "") {
      addPassword(newPassword);
      setShowForm(!showForm);
      setValues({ password: "", account: "" });
    } else {
      console.log("write");
    }
  };

  if (!user) {
    navigate("/login");
  }

  return (
    <div className="container">
      <div className="dashboard-title">
        <h3>Your password</h3>
        <button className="showForm" onClick={() => setShowForm(!showForm)}>
          +
        </button>
      </div>
      {showForm === true && (
        <>
          <PasswordForm
            setValue={(name: string, value: string) => {
              setValues((v) => {
                return {
                  ...v,
                  [name]: value,
                };
              });
            }}
            values={values}
            clickForm={addNewPassword}
          />
        </>
      )}
      {passwords && (
        <div className="password-wrap">
          {Object.values(passwords).map((el: any) => {
            return (
              <PasswordCard
                key={el.id}
                id={el.id}
                account={el.account}
                password={el.password}
                setDelete={() => {
                  removePassword(el.id);
                }}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};

export default DashboardPage;
